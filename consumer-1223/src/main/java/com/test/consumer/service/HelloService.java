package com.test.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;

@FeignClient(value = "SERVICE-PROVIDER")
public interface HelloService {
    @RequestMapping("/hello")
    String hello(@RequestParam("name") String name);

    @RequestMapping("/map")
    HashMap map();
}
