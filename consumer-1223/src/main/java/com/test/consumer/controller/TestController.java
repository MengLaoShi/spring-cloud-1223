package com.test.consumer.controller;

import com.test.consumer.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * @author 徒有琴
 */
@Controller
public class TestController {
    //    @Autowired
//    private RestTemplate restTemplate;
//
//    @Value("${my.provider.url}")
//    private String url;
    @Autowired
    HelloService helloService;

    @RequestMapping("index.html")
    public String index(String name, Model model) {
        // String result = restTemplate.getForObject(url+"/hello?name=" + name, String.class);
        String result = helloService.hello(name);
        model.addAttribute("result", result);
        // HashMap map = restTemplate.getForObject(url+"/map", HashMap.class);
        HashMap map = helloService.map();
        System.out.println(map);
        return "index";
    }
}
