package com.test.provider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 徒有琴
 */
@RestController
public class HelloController {
    @RequestMapping("hello")
    public String hello(String name) {
        return "1909:" + name;
    }

    @RequestMapping("map")
    public Map map() {
        Map map = new HashMap();
        map.put("name", "1909");
        map.put("class", "千锋的希望");
        return map;
    }
}
