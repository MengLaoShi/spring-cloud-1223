package com.test.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 徒有琴
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaStarter {
    public static void main(String[] args) {
        SpringApplication.run(EurekaStarter.class,args);
    }
}
